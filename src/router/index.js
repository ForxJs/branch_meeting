import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Fight from '@/components/Fight'
// import Hi from '@/components/hi'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/hello',
      name: 'HelloWorld',
      component: HelloWorld
    }, {
      path: '/fight',
      name: 'fight',
      component: Fight
    }, 
  //   {
  //     path: '/hi',
  //     name: 'hi',
  //     component: Hi
  //   }
  ]
})
